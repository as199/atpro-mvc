<?php
session_start();


use Atpro\mvc\Config\utils\Import;
use Atpro\mvc\core\Application;
use Dotenv\Dotenv;

require_once dirname(__DIR__). '/vendor/autoload.php';
$dotenv = Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();
require_once dirname(__DIR__). Import::helper();

//#region gestion du routing du projet
Application::start(
    dirname(__DIR__) .WEB,
    dirname(__DIR__) .API
);

//#endregion
