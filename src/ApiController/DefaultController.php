<?php

namespace App\ApiController;

use App\Entity\Utilisateur;
use Atpro\mvc\Config\services\AtproUpload;
use Atpro\mvc\Config\services\Jwt;
use Atpro\mvc\core\AbstractController;
use JsonException;

class DefaultController extends AbstractController
{
    /**
     * @throws JsonException
     */
    public function listes(): void
    {
        if (isGet()) {
            $test = new  Utilisateur();
            $testes = json_decode(json_encode($test->findAll(), JSON_THROW_ON_ERROR), true, 512, JSON_THROW_ON_ERROR);
            echo json_encode($testes, JSON_THROW_ON_ERROR);
        } else {
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"], JSON_THROW_ON_ERROR);
        }
        exit();
    }

    /**
     * @throws JsonException
     */
    public function liste($id): void
    {
        if (isGet()) {
            $test = new  Utilisateur();
            $testes = json_decode(json_encode($test->find($id), JSON_THROW_ON_ERROR), true, 512, JSON_THROW_ON_ERROR);
            echo json_encode($testes, JSON_THROW_ON_ERROR);
        } else {
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"], JSON_THROW_ON_ERROR);
        }
        exit();
    }

    /**
     * @throws JsonException
     */
    public function delete($id): void
    {
        if (isDelete()) {
            $test = new  Utilisateur();
            $testes =  $test->delete($id);
            if ($testes === 1) {
                http_response_code(200);
                echo json_encode(["message" => "supprimer avec success"], JSON_THROW_ON_ERROR);
            } else {
                http_response_code(503);
                echo json_encode(["message" => "La suppression n'a pas été effectuée"], JSON_THROW_ON_ERROR);
            }
        } else {
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"], JSON_THROW_ON_ERROR);
        }
        exit();
    }

    /**
     * @throws JsonException
     */
    public function add(): void
    {
        if (isPost()) {
            $image = AtproUpload::upload('avatar');
            $donnees = json_decode(json_encode($_POST, JSON_THROW_ON_ERROR), false, 512, JSON_THROW_ON_ERROR);
            $test = new  Utilisateur();
            $test->setPrenom($donnees->prenom);
            $test->setEmail($donnees->email);
            $test->setPassword($donnees->password);
            $test->setStatus(true);
            $test->setRole($donnees->role);
            $test->setAvatar($image);
            if ($test->create(['passwordConfirm'])) {
                http_response_code(201);
                echo json_encode($test, JSON_THROW_ON_ERROR);
            } else {
                http_response_code(503);
                echo json_encode(["message" => "La creation n'a pas été effectuée"], JSON_THROW_ON_ERROR);
            }
        } else {
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"], JSON_THROW_ON_ERROR);
        }
        exit();
    }

    /**
     * @throws JsonException
     */
    public function login(): void
    {
        if (isPost()) {
            $donnees = json_decode(file_get_contents("php://input"), false, 512, JSON_THROW_ON_ERROR);
            $test = new  Utilisateur();
            $email =$donnees->email;
            $password = $donnees->password;
            $res = $test->authApi($email, $password);
            if ($res !== null) {
                $header = headerPayload();
                $payload = setPayload($res->id, [$res->role], $res->email, ['prenom'=>$res->prenom]);
                $jwt = new Jwt();
                $token = $jwt->generate($header, $payload);
                http_response_code(201);
                echo json_encode(["token" => $token], JSON_THROW_ON_ERROR);
            } else {
                http_response_code(503);
                echo json_encode(["message" => "Invalid credentials"], JSON_THROW_ON_ERROR);
            }
        } else {
            http_response_code(405);
            echo json_encode(["message" => "La méthode n'est pas autorisée"], JSON_THROW_ON_ERROR);
        }
        exit();
    }


    public function getAccess(): array
    {
        return [
            'listes'=>[],
            'add'=>['ROLE_ADMIN'],
            'delete'=>['ROLE_ADMIN']
        ];
    }
}
