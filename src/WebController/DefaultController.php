<?php

namespace App\WebController;

use App\Entity\Test;
use Atpro\mvc\core\AbstractController;

class DefaultController extends AbstractController
{
    public function home()
    {
        return  $this->render('home/accueil', ['name' => 'FRAMEWORK ATPRO-MVC']);
    }
   

    /**
     * @return array
     */
    public function getAccess(): array
    {
        return [];
    }
}
