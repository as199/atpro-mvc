<?php

namespace GlobalHelpers\middlewares;

abstract class BaseMiddleware
{
    abstract public function execute();
}
