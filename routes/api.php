<?php
/**
 * @example $apiRouter->get('/api/users', 'DefaultController@listes');
 */
$apiRouter->get('/api/users', 'DefaultController@listes');
$apiRouter->get('/api/users/:id', 'DefaultController@liste');
$apiRouter->delete('/api/users/:id', 'DefaultController@delete');
$apiRouter->post('/api/users', 'DefaultController@add');
$apiRouter->post('/api/login', 'DefaultController@login');
$apiRouter->post('/api/users', 'DefaultController@add');
