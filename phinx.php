<?php

$hostname = 'localhost';
$port = 3306;
$dbname = 'ATPRO-MVC';
$dbpassword='pass';
$dbuser= 'root';
$driver = 'mysql';

if($driver=='mysql'){
    $pdo = new PDO(
    'mysql:host='. $hostname.':'.$port.';dbname='.$dbname,
    $dbuser,
    $dbpassword,
    [
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ]
);
}else{
     $pdo = new PDO(
    'pgsql:host='.$hostname.';
            port='.$port.';dbname='.$dbname.';
            user='.$dbuser.';
            password='.$password
);
}

return [
    'paths' => [
        'migrations'=> __DIR__.'/db/migrations',
        'seeds'=> __DIR__.'/db/seeds'
    ],
    'environments' => [
        'default_database' => 'development',
        'development' => [
            'name' => $dbname,
            'connection' => $pdo
        ]
    ]
];
