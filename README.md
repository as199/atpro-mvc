
# ATPRO-MVC FRAMEWORK

ATPRO MVC est une framework php. Il utilise l'orienté objet en php pour permettre une simple utilisation.
il utilise comment moteur de template Twig template engine et pour gestionnaire de base de donnée Phinx migration manager
.
![Image text](https://logotypemaker.com/logo-previews/lr_ckthds49p0002wietpkcypqdm/logo.png)

## Présentation
### Architectures des dossiers
![Image text](https://github.com/as199/Recipe-atpro-mvc-framework/blob/master/ATPRO-MVC-IMAGE/architectures.png?raw=true)

### le dossier db
***
le dossier **db** contient les migrations et les seeds générer avec **PHINX** pour la creation et le remplissage en masse des Tables de la base de donnée.
Pour le creation de migration et de seeds veuillez vous référer à la documentation au niveau de **[Phinx](https://phinx.org/)**

### le dossier helpers
***
le dossier **helpers** contient les extensions de Twig et les middlewares de notre application.

### le dossier public
***
le dossier **public** contient le ficher d'entré de notre application et contient tous les assets(css, js, etc ...)

### le dossier routes
***
le dossier **routes** contient deux fichiers : **web.php** et **api.php** :

Le **web.php** contiendras tous les routes de notre web application.

Le **api.php** contiendras tous les routes de notre api application.
***
### le dossier src
***
le dossier **src** contient un dossier **Entity** ,**ApiController** et **WebController**:

**A. le dossier Entity**: ce dossier contiendra l'ensemble des entités(models) de notre application.

NB: chaque entité doit obligatoirement étendre de l'**AbstractController** et implementer sa méthode **getAccess** .

***
**B. le dossier ApiController et WebController**: contiendrons respectivement les controllers de notre **API** ou de notre **web** application.
***

### le dossier views
il contiendra l'ensemble des vue de notre application et contient un dossier **layouts** qui contiendrons nos différents layouts.

**NB: ** Toutes nos vue aurons comme extension **.twig**

***
### le fichier .env

il contient l'ensemble des variables de configuration de l'application.

** les variables de connection à la base de donnée**, **la clé secrete poru la génération du token** et ** le token TTL ** pour la durée de vie du token.
