<?php


use Phinx\Seed\AbstractSeed;

class FillTestTables extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'prenom'=> 'Assane Dione',
                'email'=> 'dioneassane0290@gmail.com',
                'password'=> password_hash('pass123', PASSWORD_ARGON2I),
                'status'=> 1,
                'role'=> 'ROLE_ADMIN',
            ],[
                'prenom'=> 'Fatou Sene',
                'email'=> 'fatou@gmail.com',
                'password'=> password_hash('pass123', PASSWORD_ARGON2I),
                'status'=> 1,
                'role'=> 'ROLE_USER',
            ]
        ];
        $this->table('test')->insert($data)->saveData();
    }
}
